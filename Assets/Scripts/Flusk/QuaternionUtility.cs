using UnityEngine;

namespace Flusk
{
    public static class QuaternionUtility
    {
        public static Quaternion Difference(Quaternion lhs, Quaternion rhs)
        {
            var lhsInverse = Quaternion.Inverse(lhs);
            return lhsInverse * rhs;
        }
    }
}