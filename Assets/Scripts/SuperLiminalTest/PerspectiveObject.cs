﻿using System;
using UnityEngine;

public class PerspectiveObject : MonoBehaviour
{
    public float ScaleDistance { get; private set; }

    public Vector3 Center => transform.position + boxCollider.center;

    public Vector3 HalfExtents => boxCollider.size;

    public Quaternion Orientation => transform.localRotation;

    public bool CanScale { get; set; }

    public event Action OnCollidedWithRoomSurface;

    private BoxCollider boxCollider;

    private Vector3 initialScale;

    private new Rigidbody rigidbody;

    private Transform focusCamera;

    private bool isControlledByPlayer;

    private Quaternion initialRotation;

    private Vector3 initialDirection;

    private Transform originalParent;

    private float initialDistance;
    
    public void PrepareInteraction(float distance, Camera camera)
    {
        ScaleDistance = distance;
        initialScale = transform.localScale;
        rigidbody.isKinematic = true;
        CanScale = true;
        focusCamera = camera.transform;
        isControlledByPlayer = true;
        initialRotation = transform.localRotation;
        initialDirection = (focusCamera.position - transform.position).normalized;

        initialDistance = Vector3.Distance(transform.position, camera.transform.position);
    }

    public void EndInteraction()
    {
        rigidbody.isKinematic = false;
        isControlledByPlayer = false;
        transform.parent = originalParent;
    }

    public float CompareDistance(float currentDistance)
    {
        return currentDistance / ScaleDistance;
    }

    public void ScaleByDistance(float currentDistance)
    {
        if (!CanScale)
        {
            return;
        }
        var scale = CompareDistance(currentDistance);
        transform.localScale = initialScale * scale;
    }

    public void InteractWithAgent(bool state, Collider otherCollider)
    {
        Physics.IgnoreCollision(boxCollider, otherCollider, state);
    }

    public void UpdateRotation(Quaternion rotation)
    {
        //transform.localRotation = initialRotation * rotation;
    }

    private void OnCollisionEnter(Collision collision)
    {
        var roomCollider = collision.gameObject.GetComponent<RoomCollider>();
        if (roomCollider != null)
        {
            CanScale = false;
            OnCollidedWithRoomSurface?.Invoke();
        }
    }

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        rigidbody = GetComponent<Rigidbody>();
        CanScale = true;
    }
}
