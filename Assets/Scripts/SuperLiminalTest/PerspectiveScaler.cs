﻿using System;
using Flusk;
using TMPro;
using UnityEngine;

namespace SuperLiminalTest
{
    public class PerspectiveScaler : MonoBehaviour
    {
        [SerializeField]
        protected Camera perspectiveCamera;

        [SerializeField]
        protected TextMeshProUGUI text;

        [SerializeField] protected LayerMask perspectiveObjectMask;

        [SerializeField] protected LayerMask roomColliderMask;

        [SerializeField] protected CapsuleCollider playerCollider;

        private enum PerspectiveState
        {
            Searching,
            Scaling
        }

        private PerspectiveState currentPerspectiveState;

        private PerspectiveObject currentPerspectiveObject;

        private Collider roomCollider;

        private Quaternion selectRotation;

        private void FixedUpdate()
        {
            switch (currentPerspectiveState)
            {
                case PerspectiveState.Searching:
                {
                    Search();
                    break;
                }
                case PerspectiveState.Scaling:
                {
                    Scaling();
                    break;
                }

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Scaling()
        {
            if (Input.GetMouseButtonUp(0))
            {
                currentPerspectiveState = PerspectiveState.Searching;
                roomCollider = null;

                if (currentPerspectiveObject != null)
                {
                    currentPerspectiveObject.EndInteraction();
                    currentPerspectiveObject.InteractWithAgent(true, playerCollider);
                    currentPerspectiveObject.OnCollidedWithRoomSurface -= PerspectiveObjectCollidedWithRoomSurface;
                    currentPerspectiveObject = null;
                }

                
                text.text = "Current Perspective: Nothing";
                return;
            }

            var origin = perspectiveCamera.transform.position;
            var halfExtents = currentPerspectiveObject.HalfExtents;
            var direction = perspectiveCamera.transform.forward;
            RaycastHit hitInfo;
            var orientation = currentPerspectiveObject.Orientation;

            bool boxHit = Physics.BoxCast(origin, halfExtents, direction, out hitInfo, orientation, float.MaxValue,
                roomColliderMask);

            Ray ray = new Ray(origin, direction);
            RaycastHit rayHitInfo;
            Physics.Raycast(ray, out rayHitInfo, float.MaxValue, roomColliderMask);

            if (!boxHit)
            {
                roomCollider = null;
                return;
            }

            roomCollider = hitInfo.collider;
            // Calculate scale

            // Calculate closest position
            var hitPosition = rayHitInfo.point;
            halfExtents = currentPerspectiveObject.HalfExtents;

            var placement = hitPosition - halfExtents.magnitude * direction;
            currentPerspectiveObject.transform.position = placement;
            
            currentPerspectiveObject.
                ScaleByDistance(Vector3.Distance(currentPerspectiveObject.transform.position, transform.position));
            
            // Difference in Rotation
            var difference =
                QuaternionUtility.Difference(selectRotation, perspectiveCamera.transform.rotation);
            
            currentPerspectiveObject.UpdateRotation(difference);    
        }

        private void Search()
        {
            var cameraTransform = perspectiveCamera.transform;
            Vector3 direction = cameraTransform.forward;
            Ray ray = new Ray(cameraTransform.position, direction);
            RaycastHit hitInfo;
            bool hasHit = Physics.Raycast(ray, out hitInfo);
            if (hasHit && Input.GetMouseButton(0))
            {
                currentPerspectiveObject = hitInfo.transform.gameObject.GetComponent<PerspectiveObject>();
                if (currentPerspectiveObject == null)
                {
                    return;
                }

                currentPerspectiveState = PerspectiveState.Scaling;
                text.text = "Current Perspective: " + currentPerspectiveObject.name;
                currentPerspectiveObject.PrepareInteraction(hitInfo.distance, perspectiveCamera);
                currentPerspectiveObject.OnCollidedWithRoomSurface += PerspectiveObjectCollidedWithRoomSurface;
                currentPerspectiveObject.InteractWithAgent(false, playerCollider);
                selectRotation = transform.rotation;
                
                // Less elegant, uninteresting hack
                // Parent the object to the camera
                currentPerspectiveObject.transform.parent = perspectiveCamera.transform;
            }
        }

        private void PerspectiveObjectCollidedWithRoomSurface()
        {
            
        }
    }
}
